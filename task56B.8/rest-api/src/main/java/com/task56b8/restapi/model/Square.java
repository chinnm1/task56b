package com.task56b8.restapi.model;

public class Square extends Rectangle {
    private double side;

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(String color, boolean filled, double side) {
        super(color, filled);
        this.side = side;
    }

    public Square(double width, double length) {
        super(width, length);
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public void setWidth(double width) {
        // TODO Auto-generated method stub
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        // TODO Auto-generated method stub
        super.setLength(length);
    }

    @Override
    public String toString() {
        return String.format("Square [ Rectangle [Shape [color =%s, filled= %s], width =%s, length = %S]", getColor(),
                isFilled(), getWidth(), getLength());
    }
}
