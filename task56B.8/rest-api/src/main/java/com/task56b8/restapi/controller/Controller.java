package com.task56b8.restapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56b8.restapi.model.Circle;
import com.task56b8.restapi.model.Rectangle;
import com.task56b8.restapi.model.Square;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea(@RequestParam(value = "radius") double radius) {
        Circle circle = new Circle(radius);
        return circle.getArea();
    }

    @GetMapping("/circle-perimeter")
    public double getCirclePerimeter(@RequestParam(value = "radius") double radius) {
        Circle circle = new Circle(radius);
        return circle.getParimeter();
    }

    @GetMapping("/rectangle-area")
    public double getRectangleArea(@RequestParam(value = "width") double width,
            @RequestParam(value = "height") double height) {
        Rectangle rectangle = new Rectangle(width, height);
        return rectangle.getArea();
    }

    @GetMapping("/rectangle-perimeter")
    public double getRectanglePerimeter(@RequestParam(value = "width") double width,
            @RequestParam(value = "height") double height) {
        Rectangle rectangle = new Rectangle(width, height);
        return rectangle.getPerimeter();
    }

    @GetMapping("/square-area")
    public double getSquareArea(@RequestParam(value = "width") double width,
            @RequestParam(value = "height") double height) {
        Square square = new Square(width, height);
        return square.getArea();
    }

    @GetMapping("/square-perimeter")
    public double getSquarePerimeter(@RequestParam(value = "width") double width,
            @RequestParam(value = "height") double height) {
        Square square = new Square(width, height);
        return square.getPerimeter();
    }

}
