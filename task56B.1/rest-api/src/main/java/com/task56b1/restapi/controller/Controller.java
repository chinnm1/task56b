package com.task56b1.restapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56b1.restapi.model.Circle;
import com.task56b1.restapi.model.Cylinder;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/circle-area")
    public double getCircleArea(@RequestParam(value = "radius") double radius) {
        Circle circle = new Circle(radius);
        return circle.getArea();
    }

    @GetMapping("/cylinder-volume")
    public double getCylinderVolume(@RequestParam(value = "radius") double radius,
            @RequestParam(value = "height") double height) {
        Cylinder cylinder = new Cylinder(radius, height);
        return cylinder.getVolume();
    }

}
