package com.task56b5.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56b5.restapi.model.Account;
import com.task56b5.restapi.model.Customer;

import ch.qos.logback.core.joran.action.Action;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getAccount() {
        Customer customer1 = new Customer(1, "Mai", 10);
        Customer customer2 = new Customer(2, "lan", 15);
        Customer customer3 = new Customer(3, "Truc", 20);

        Account account1 = new Account(1, customer1, 100000);
        Account account2 = new Account(1, customer2, 150000);
        Account account3 = new Account(1, customer3, 200000);
        ArrayList<Account> arrayList = new ArrayList<>();
        arrayList.add(account1);
        arrayList.add(account2);
        arrayList.add(account3);
        return arrayList;

    }

}
