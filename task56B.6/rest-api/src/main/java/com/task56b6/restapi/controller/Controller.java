package com.task56b6.restapi.controller;

import java.sql.Date;
import java.util.ArrayList;

import javax.xml.crypto.Data;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56b6.restapi.model.Customer;
import com.task56b6.restapi.model.Visit;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/visits")
    public ArrayList<Visit> getVisit() {
        Customer customer1 = new Customer("Mai");
        Customer customer2 = new Customer("Lan");
        Customer customer3 = new Customer("Truc");

        Date date1 = new Date(13 / 12 / 2022);
        Date date2 = new Date(12 / 12 / 2022);
        Date date3 = new Date(11 / 12 / 2022);

        Visit visit1 = new Visit(customer1, date1, 100000, 100000);
        Visit visit2 = new Visit(customer2, date2, 200000, 200000);
        Visit visit3 = new Visit(customer3, date3, 300000, 300000);
        ArrayList<Visit> arrayList = new ArrayList<>();
        arrayList.add(visit1);
        arrayList.add(visit2);
        arrayList.add(visit3);
        return arrayList;

    }

}
