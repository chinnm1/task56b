package com.task56b2.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56b2.restapi.model.Author;
import com.task56b2.restapi.model.Book;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getlistBook() {
        Author author1 = new Author("Xuan Quynh", "xuanquynh@gmail.com", 'f');
        Author author2 = new Author("To Huu", "tohuu@gmail.com", 'm');
        Author author3 = new Author("Nam Cao", "namcao@gmail.com", 'm');
        Book book1 = new Book("Sóng", author1, 100000, 1);
        Book book2 = new Book("Dế mèn phiêu lưu kí", author2, 200000, 2);
        Book book3 = new Book("Lão Hạc", author3, 300000, 3);
        ArrayList<Book> arrayList = new ArrayList<>();
        arrayList.add(book1);
        arrayList.add(book2);
        arrayList.add(book3);
        return arrayList;

    }

}
