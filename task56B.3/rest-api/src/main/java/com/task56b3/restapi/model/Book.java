package com.task56b3.restapi.model;

import java.util.ArrayList;

public class Book {
    public Book(String name, ArrayList<Author> authors, double price, int qty) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
    }

    private String name;
    private ArrayList<Author> authors;

    private double price;
    private int qty = 0;

    public String getName() {
        return name;
    }

    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthors(ArrayList<Author> authors) {
        this.authors = authors;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ArrayList<Object> getAuthorName() {
        Author author1 = new Author("Xuan Quynh", "xuanquynh@gmail.com", 'f');
        Author author2 = new Author("To Huu", "tohuu@gmail.com", 'm');
        Author author3 = new Author("Nam Cao", "namcao@gmail.com", 'm');
        Author author4 = new Author("Xuan Dieu", "xuandieu@gmail.com", 'm');
        Author author5 = new Author("Ho Xuan Huong", "hõunahuong@gmail.com", 'f');
        Author author6 = new Author("Thach Lam", "thachlam@gmail.com", 'm');
        ArrayList<Author> authorList1 = new ArrayList<>();
        authorList1.add(author1);
        authorList1.add(author2);
        ArrayList<Author> authorList2 = new ArrayList<>();
        authorList2.add(author3);
        authorList2.add(author4);
        ArrayList<Author> authorList3 = new ArrayList<>();
        authorList3.add(author5);
        authorList3.add(author6);
        ArrayList<Object> listAuthor = new ArrayList<>();
        listAuthor.add(authorList1);
        listAuthor.add(authorList2);
        listAuthor.add(authorList3);
        return listAuthor;

    }

}
