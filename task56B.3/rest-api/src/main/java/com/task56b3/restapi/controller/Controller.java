package com.task56b3.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56b3.restapi.model.Author;
import com.task56b3.restapi.model.Book;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getlistBookV2() {
        Author author1 = new Author("Xuan Quynh", "xuanquynh@gmail.com", 'f');
        Author author2 = new Author("To Huu", "tohuu@gmail.com", 'm');
        Author author3 = new Author("Nam Cao", "namcao@gmail.com", 'm');
        Author author4 = new Author("Xuan Dieu", "xuandieu@gmail.com", 'm');
        Author author5 = new Author("Ho Xuan Huong", "hõunahuong@gmail.com", 'f');
        Author author6 = new Author("Thach Lam", "thachlam@gmail.com", 'm');
        ArrayList<Author> authorList1 = new ArrayList<>();
        authorList1.add(author1);
        authorList1.add(author2);
        ArrayList<Author> authorList2 = new ArrayList<>();
        authorList2.add(author3);
        authorList2.add(author4);
        ArrayList<Author> authorList3 = new ArrayList<>();
        authorList3.add(author5);
        authorList3.add(author6);

        Book book1 = new Book("Sóng", authorList1, 100000, 1);
        Book book2 = new Book("Dế mèn phiêu lưu kí", authorList2, 200000, 2);
        Book book3 = new Book("Lão Hạc", authorList3, 300000, 3);
        ArrayList<Book> arrayList = new ArrayList<>();
        arrayList.add(book1);
        arrayList.add(book2);
        arrayList.add(book3);
        return arrayList;

    }
}
