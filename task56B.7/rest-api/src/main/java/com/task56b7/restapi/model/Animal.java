package com.task56b7.restapi.model;

public class Animal {
    String name;

    public Animal(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("Änimal [ Name =%s]", name);
    }

}
