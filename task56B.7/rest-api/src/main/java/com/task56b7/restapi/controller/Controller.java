package com.task56b7.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56b7.restapi.model.Cat;
import com.task56b7.restapi.model.Dog;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/cats")
    public ArrayList<Cat> getcats() {

        Cat cat1 = new Cat("Tam The");
        Cat cat2 = new Cat("Meo den");
        Cat cat3 = new Cat("Meo muop");
        ArrayList<Cat> arrayList = new ArrayList<>();
        arrayList.add(cat1);
        arrayList.add(cat2);
        arrayList.add(cat3);
        return arrayList;
    }

    @GetMapping("/dogs")
    public ArrayList<Dog> getdogs() {

        Dog dog1 = new Dog("Vang");
        Dog dog2 = new Dog("Muc");
        Dog dog3 = new Dog("Dom");
        ArrayList<Dog> arrayList = new ArrayList<>();
        arrayList.add(dog1);
        arrayList.add(dog2);
        arrayList.add(dog3);
        return arrayList;
    }

}
